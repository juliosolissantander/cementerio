from django.shortcuts import render
from .forms import ContactForm
from django.conf import settings
from django.core.mail import send_mail
# Create your views here.
def contacto(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_email = form. cleaned_data.get("email")
        formulario_mensaje = form.cleaned_data.get("mensaje")
        asunto = "Formulario de contacto web"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "jorge.borquez@udec.cl"]
        email_mensaje = "Enviado por: %s - Correo: %s - Mensaje: %s" % (formulario_nombre,formulario_email,formulario_mensaje)
        send_mail(asunto,
                   email_mensaje,
                   email_from,
                   email_to,
                   fail_silently = False
        )
        print(form.cleaned_data)
    contexto = {
        "el_contacto": form
    }
    return render(request, "contacto.html", contexto)

