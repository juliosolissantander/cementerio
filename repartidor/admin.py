from django.contrib import admin
from .models import RepartidorModel
from .forms import *
# Register your models here.
class AdminRepartidor(admin.ModelAdmin):
    list_display = ["id_repartidor","nombre","fono","tipo_de_licencia"]
    form = RepartidorValidator
    list_filter = ["nombre","tipo_de_licencia"]
    search_fields = ["nombre","tipo_de_licencia"]



admin.site.register(RepartidorModel, AdminRepartidor)
