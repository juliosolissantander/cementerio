
from django.shortcuts import render
from .forms import *
from .models import *
# Create your views here.

def inicio(request):
    form = RepartidorValidator(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)
    contexto = {
        "el_titulo": titulo,
        "el_formulario": form,

    }
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        rut2 = form_data.get("rut")
        fono2 = form_data.get("fono")
        mail2 = form_data.get("mail")
        genero2 = form_data.get("genero")
        tipo_de_licencia2 = form_data.get("tipo_de_licencia")

        objeto = RepartidorModel.objects.create(nombre=nombre2, rut=rut2, fono=fono2,
                                                mail=mail2, genero=genero2, tipo_de_licencia=tipo_de_licencia2)

        contexto = {
            "el_formulario": form,
        }

    return render(request, "repartidorhtml.html", contexto)
