from django import forms
from .models import RepartidorModel

class RepartidorValidator(forms.ModelForm):
    class Meta:
        model = RepartidorModel
        fields=["nombre","rut","fono","mail","genero","tipo_de_licencia"]


    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre)<5:
                raise forms.ValidationError("El Nombre no puede ser menor a 5 caracteres")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_rut(self):
        rut = self.cleaned_data.get("rut")
        if rut:
            if len(rut)<11:
                raise forms.ValidationError("El Rut no puede ser menor a 11 caracteres")
            return rut
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_fono(self):
        fono = self.cleaned_data.get("fono")

        if fono < 0:
            raise forms.ValidationError("El fono no puede ser menor a 0")
        elif fono == 0:
            raise forms.ValidationError("El fono no puede ser cero")
        return fono

    def clean_mail(self):
        mail = self.cleaned_data.get("mail")
        if mail:
            if len(mail) < 5:
                raise forms.ValidationError("Correo es demasiado corto")
            return mail
        else:
            raise forms.ValidationError("Este campo es requerido")


    def clean_genero(self):
        genero = self.cleaned_data.get("genero")
        if genero:
            if len(genero)<5:
                raise forms.ValidationError("El genero no puede ser menor a 5 caracteres")
            return genero
        else:
            raise forms.ValidationError("El genero es requerido")

    def clean_tipo_de_licencia(self):
        tipo_de_licencia = self.cleaned_data.get("tipo_de_licencia")
        if tipo_de_licencia:
            if len(tipo_de_licencia)<0:
                raise forms.ValidationError("El tipo de licencia no puede ser menor a 1 caracteres, Ejemplo A, B, C, No tiene")
            return tipo_de_licencia
        else:
            raise forms.ValidationError("El tipo de licencia es requerida")