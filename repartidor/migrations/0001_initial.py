# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-15 18:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RepartidorModel',
            fields=[
                ('id_repartidor', models.AutoField(primary_key=True, serialize=False, unique=True)),
                ('nombre', models.CharField(max_length=80)),
                ('rut', models.CharField(max_length=12)),
                ('fono', models.IntegerField()),
                ('genero', models.CharField(max_length=10)),
                ('tipo_de_licencia', models.CharField(max_length=8)),
            ],
        ),
    ]
