from django.db import models

# Create your models here.
class RepartidorModel(models.Model):
    id_repartidor = models.AutoField(primary_key=True,unique=True)
    nombre = models.CharField(max_length=80)
    rut = models.CharField(max_length=12)
    fono = models.IntegerField()
    mail = models.CharField(max_length=80)
    genero = models.CharField(max_length=10,choices=(('Femenino','Femenino'),('Masculino','Masculino')),default=1)
    tipo_de_licencia = models.CharField(max_length=8,choices=(('A','A'),('B','B'),('No tiene','No tiene')),default=1)