from django import forms
from .models import VehiculoModel

class VehiculoValidator(forms.ModelForm):
    class Meta:
        model = VehiculoModel
        fields = ["tipo_vehiculo", "marca", "modelo", "anno", "cilindrada", "patente", "repartidor_a_cargo"]

    def clean_tipo_vehiculo(self):
        tipo_vehiculo = self.cleaned_data.get("tipo_vehiculo")
        if tipo_vehiculo:
            if len(tipo_vehiculo) < 4:
                raise forms.ValidationError("El Tipo de Vehiculo no puede ser menor a 5 caracteres")
            return tipo_vehiculo
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_marca(self):
        marca = self.cleaned_data.get("marca")
        if marca:
            if len(marca) < 5:
                raise forms.ValidationError("La Marca no puede ser menor a 5 caracteres")
            return marca
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_modelo(self):
        modelo = self.cleaned_data.get("modelo")
        if modelo:
            if len(modelo) < 5:
                raise forms.ValidationError("El Modelo no puede ser menor a 5 caracteres")
            return modelo
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_anno(self):
        anno = self.cleaned_data.get("anno")

        if anno < 2012:
            raise forms.ValidationError("El año no puede ser menor a 2012")
        elif anno == 0:
            raise forms.ValidationError("El año no puede ser cero")
        elif anno > 2018:
            raise forms.ValidationError("El año no puede mayo al actual")
        return anno

    def clean_cilindrada(self):
        cilindrada = self.cleaned_data.get("cilindrada")

        if cilindrada < 0:
            raise forms.ValidationError("La cilindrada no puede ser menor a 0")
        elif cilindrada == 0:
            raise forms.ValidationError("La cilindrada no puede ser cero")
        return cilindrada

    def clean_patente(self):
        patente = self.cleaned_data.get("patente")
        if patente:
            if len(patente) < 2:
                raise forms.ValidationError("La patente no puede ser de un caracter")
            return patente
        else:
            raise forms.ValidationError("La patente es requerida")

    def clean_repartidor_a_cargo(self):
        repartidor_a_cargo = self.cleaned_data.get("repartidor_a_cargo")
        if repartidor_a_cargo:
            if len(repartidor_a_cargo) < 5:
                raise forms.ValidationError("La patente no puede ser menor a 5 caracteres")
            return repartidor_a_cargo
        else:
            raise forms.ValidationError("La patente es requerida")