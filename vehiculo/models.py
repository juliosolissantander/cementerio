from django.db import models

# Create your models here.
class VehiculoModel(models.Model):
    id_vehiculo = models.AutoField(primary_key=True,unique=True)
    tipo_vehiculo = models.CharField(max_length=80,choices=(('moto','moto'),('camioneta','camioneta')),default=1)
    marca = models.CharField(max_length=80)
    modelo = models.CharField(max_length=80)
    anno = models.IntegerField()
    cilindrada = models.IntegerField()
    patente = models.CharField(max_length=6)
    repartidor_a_cargo = models.CharField(max_length=80)