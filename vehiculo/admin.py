from django.contrib import admin
from .models import VehiculoModel
from .forms import *
# Register your models here.
class AdminVehiculo(admin.ModelAdmin):
    list_display = ["id_vehiculo","tipo_vehiculo","patente","repartidor_a_cargo"]
    form =VehiculoValidator
    list_filter = ["patente","tipo_vehiculo"]
    search_fields = ["patente","tipo_vehiculo"]



admin.site.register(VehiculoModel, AdminVehiculo)

