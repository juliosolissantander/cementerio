from django.shortcuts import render
from .forms import *
from .models import *
# Create your views here.

def inicio(request):
    form = VehiculoValidator(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)
    contexto = {
        "el_titulo": titulo,
        "el_formulario": form,

    }
    if form.is_valid():
        form_data = form.cleaned_data
        tipo_vehiculo2 = form_data.get("tipo_vehiculo")
        marca2 = form_data.get("marca")
        modelo2 = form_data.get("modelo")
        anno2 = form_data.get("anno")
        cilindrada2 = form_data.get("cilindrada")
        patente2 = form_data.get("patente")
        repartidor_a_cargo2 = form_data.get("repartidor_a_cargo")

        objeto = VehiculoModel.objects.create(tipo_vehiculo=tipo_vehiculo2, marca=marca2, modelo=modelo2,
                                              anno=anno2, cilindrada=cilindrada2, patente=patente2,
                                              repartidor_a_cargo=repartidor_a_cargo2)

        contexto = {
            "el_formulario": form,
        }

    return render(request, "vehiculohtml.html", contexto)
